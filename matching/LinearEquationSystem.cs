using System;
using System.Collections.Generic;

namespace Qoniac.Common.MathHelpers
{
    /// <summary>
    /// Simpler (Pivot-/Gauss-)L�ser f�r lineare Gleichungssysteme der Form 0=ai + bi*x1 + ci*x2 + ...
    /// </summary>
	public class LinearEquationSystem
	{
        /// <summary>
        /// Ergebnis eines L�sungsversuches
        /// </summary>
		public enum SolveErg
		{
            /// <summary>
            /// Es gibt im reellen Bereich keine g�ltige L�sung
            /// </summary>
			NoSolution,

            /// <summary>
            /// Es gibt eine eindeutige L�sung
            /// </summary>
			OneSolution,
            
            /// <summary>
            /// Es gibt mehrere L�sungen
            /// </summary>
			ManySolutions
		}

        /// <summary>
        /// Parameter des Gleichungssystems
        /// </summary>
		private double[] arr;

        /// <summary>
        /// Breite des Gleichungssystems, d.h. Anzahl der Variablen + 1 (konstantes Element)
        /// </summary>
		public int Width{get; private set;}

        /// <summary>
        /// H�he des Gleichungssystems, d.h. Anzahl der Gleichungen
        /// </summary>
		public int Height{get;private set;}

        /// <summary>
        /// Bestimmt zu einer Zeile und Spalte den eindimensionalen Index im Array
        /// </summary>
        /// <param name="zeile">Index der Gleichung</param>
        /// <param name="spalte">Index des Parameters in der Gleichung</param>
        /// <returns>eindimensionaler Index</returns>
		private int index(int zeile, int spalte)
		{
			return zeile*Width+spalte;
		}

        /// <summary>
        /// Parameter des Gleichungssystems
        /// </summary>
        /// <param name="row">Indiziert die einzelne Gleichung</param>
        /// <param name="column">Indiziert einen Parameter in der Gleichung (0=konstantes Element, 1=1.Parameter,...)</param>
        /// <returns>Wert des Parameters</returns>
		public double this[int row, int column]
		{
			get
			{
				return arr[index(row,column)];
			}
			set
			{
				arr[index(row,column)]=value;
			}
		}
        /// <summary>
        /// Erstellt ein neues lineares Gleichungssystem der Form 0=ai + bi*x1 + ci*x2 + ...
        /// </summary>
        /// <param name="width">Breite, d.h. Anzahl der Variablen+1</param>
        /// <param name="height">Anzahl der Gleichungen</param>
		public LinearEquationSystem(int width, int height)
		{
			this.Width=width;
			this.Height=height;
			arr=new double[width*height];
            Ergs = new double[Width - 1];
		}

        /// <summary>
        /// Ergebnisse des L�sens:
        /// x1= Ergs[0], x2=Ergs[1], ...
        /// </summary>
        public double[] Ergs { get; private set; }

        /// <summary>
        /// Copy-Konstruktor
        /// </summary>
        /// <param name="rhs">zu kopierendes Gleichungssystem</param>
		private LinearEquationSystem(LinearEquationSystem rhs)
		{
			this.Width=rhs.Width;
			this.Height=rhs.Height;
            arr = (double[]) rhs.arr.Clone();
            Ergs = rhs.Ergs; //nicht n�tig, zu kopieren, das wollen wir ja nutzen
		}
        /// <summary>
        /// L�st das Gleichungssystem und erh�lt die Parameter dabei
        /// </summary>
        /// <remarks>Die Ergebnisse finden sich hinterher in <see cref="Ergs"/></remarks>
        /// <returns>Erfolg der L�sung</returns>
		public SolveErg SolveNoDestroy()
		{
            //kopieren und da l�sen
            LinearEquationSystem tbl = new LinearEquationSystem(this);
            return tbl.SolveDestroy(); //unsere Ergs zeigen auf das selbe Array 
		}	
        /// <summary>
        /// L�st das Gleichungssystem, die Parameter werden dabei zerst�rt
        /// </summary>
        /// <remarks>Die Ergebnisse finden sich hinterher in <see cref="Ergs"/></remarks>
        /// <returns>Erfolg der L�sung</returns>
        public SolveErg SolveDestroy()
		{
            //ein Spaltenvektor beschreibt uns, welches x in der Zeile steht
            //0 => y-Wert
            //alle werden implizit auf 0 initialisiert
            int[] zeilen = new int[Height];

            //ein anderer Vektor die Spalten, die wir nicht umstellen konnten
            bool xsLeftOver = false;

            //wir versuchen, spaltenweise umzustellen, und jeweils eine noch nicht besetzte Zeile!=0 zu finden
            for (int spalte = 1; spalte < Width; ++spalte)
            {
                //wir suchen die erste passende Zeile, mit Eintrag!=0 und zeilen[i]!=0
                int zeile;
                for (zeile = 0; zeile < Height; ++zeile)
                    if (zeilen[zeile] == 0 && this[zeile, spalte] != 0)
                        break;

                if (zeile >= Height)
                    //nix gefunden
                    xsLeftOver = true;
                else 
                {
                    zeilen[zeile] = spalte;

                    //das ist unser pivot-Element
                    double neg_pivot_inverse = -1.0 / this[zeile, spalte];

                    //****************
                    // Spalteneleminierung: Spalten, wo y drin steht, interessieren nicht mehr...
                    // wir schreiben weder die neue pivot-spalte (weil nur Faktor f�r nicht 
                    // umgestelltes x oder Faktor f�r Hilfs-y, nicht relevant)
                    //****************
                    //neues Pivot-Element:
                    //this[zeile,spalte]=1.0/pivot

                    //neue Pivot-Zeile:
                    //for(int i=0; i<spalte;++i)
                    //    this[zeile,i]/=(-pivot);
                    this[zeile, 0] *= neg_pivot_inverse;
                    for (int i = spalte + 1; i < Width; ++i)
                        this[zeile, i] *= neg_pivot_inverse;

                    //Rest, unterteilt in die 4 Quadranten
                    //for(int x=0; x<spalte;++x)
                    //    for(int y=0;y<zeile;++y)
                    //        this[y,x]+=this[y,spalte]*this[zeile,x];
                    for (int y = 0; y < zeile; ++y)
                        this[y, 0] += this[y, spalte] * this[zeile, 0];
                    for (int x = spalte + 1; x < Width; ++x)
                        for (int y = 0; y < zeile; ++y)
                            this[y, x] += this[y, spalte] * this[zeile, x];
                    //for(int x=0; x<spalte;++x)
                    //	for(int y=zeile+1;y<Height;++y)
                    //		this[y,x]+=this[y,spalte]*this[zeile,x];
                    for (int y = zeile + 1; y < Height; ++y)
                        this[y, 0] += this[y, spalte] * this[zeile, 0];
                    for (int x = spalte + 1; x < Width; ++x)
                        for (int y = zeile + 1; y < Height; ++y)
                            this[y, x] += this[y, spalte] * this[zeile, x];


                    //neue pivot-spalte
                    //	for(int i=0; i<zeile;++i)
                    //		this[i,spalte]/=pivot;
                    //	for(int i=zeile+1;i<Height;++i)
                    //		this[i,spalte]/=pivot;
                }
            }

            //fertig, jetzt Ergebnis interpretieren
            //1. Gleichungssystem l�sbar (d.h. keine sich wiedersprechenden Gleichungen)
            //Fall 1: 
            //->es existiert noch ein nicht umgestelltes y 
            //Fall 1.1. der Konstant-Wert==0
            //          ->ok
            //Fall 1.2. ein anderer Wert ->0=1 ->Wiederspruch -> nicht l�sbar
            //Fall 2: alle y's umgestellt -> l�sbar
            //bei der Gelegenheit gleich die Ergebnisse eintragen
            for (int zeile = 0; zeile < Height; ++zeile)
                if (zeilen[zeile] == 0)
                {
                    //nicht umgestelltes y
                    if (this[zeile, 0] != 0)
                        return SolveErg.NoSolution;
                }
                else
                    //Ergebnis von x eintragen
                    Ergs[zeilen[zeile] - 1] = this[zeile, 0];
            if (xsLeftOver)
                return SolveErg.ManySolutions;

            return SolveErg.OneSolution;
		}
    }
}
