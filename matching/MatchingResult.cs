﻿using System;
using System.Collections.Generic;

namespace matching
{
    /// <summary>
    /// Das Ergebnis eines eines Matchings.
    /// </summary>
    class MatchingResult : IComparable<MatchingResult>
    {
        /// <summary>
        /// Die errechnete Transformationsmatrix.
        /// </summary>
        public Homography Homography { get; private set; }

        /// <summary>
        /// Die Liste der gefunden Korrespondenzen. Gespeichert werden die
        /// Regions als KeyValuePair.
        /// </summary>
        public List<KeyValuePair<Region, Region>> Mapping { get; private set; }

        /// <summary>
        /// Der Wert der Abweichung zwischen 0 (identisch) und 1 (komplett
        /// verschieden)
        /// </summary>
        public double Deviation { get; private set; }

        /// <summary>
        /// Erzeugt ein Matching-Ergebnis anhand der übergebenen Parameter.
        /// </summary>
        /// <param name="mapping">Die Liste der korrespondieren Regions.</param>
        /// <param name="homography">Die Transformationsmatrix.</param>
        /// <param name="deviation">Die Abweichung zwischen den Bildern.</param>
        public MatchingResult(List<KeyValuePair<Region,Region>> mapping,
            Homography homography, double deviation)
        {
            Mapping = mapping;
            Homography = homography;
            Deviation = deviation;
        }

        /// <summary>
        /// Implementierung von ICompareable. Ordnet die Elemente aufsteigend
        /// nach dem Wert der Abweichung.
        /// </summary>
        /// <param name="mr">Ein zu vergleichendes Matching-Ergebnis</param>
        /// <returns>0, bei gleicher Abweichung; 1, wenn die Abweichung des
        /// übergebenen Objekts kleiner ist; -1, wenn die Abweichung diese
        /// Objekts kleiner ist.</returns>
        public int CompareTo(MatchingResult mr)
        {
            if (Deviation < mr.Deviation)
            {
                return -1;
            }
            else if (Deviation == mr.Deviation)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
    }
}
