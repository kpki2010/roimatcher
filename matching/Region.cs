﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace matching
{
    /// <summary>
    /// Repräsentation einer Bildregion.
    /// </summary>
    class Region : IComparable
    {
        /// <summary>
        /// Schwerpunkt der Region.
        /// </summary>
        public Point CenterOfGravity { get; private set; }

        /// <summary>
        /// kleinstes unschließendes Rechteck.
        /// </summary>
        public Rectangle BoundingBox { get; private set; }
        
        /// <summary>
        /// Anzahl der Bildpunkte.
        /// </summary>
        public int NumberOfPoints { get; private set; }

        /// <summary>
        /// Mittelpunkt des umschleißenden Kreises.
        /// </summary>
        public Point BoundingSphereCenter { get; private set; }

        /// <summary>
        /// Radius des Kleinsten umschließenden Kreises
        /// </summary>
        public int BoundingSphereRadius { get; private set; }

        /// <summary>
        /// Nördlichster Punkt der Region.
        /// </summary>
        public Point NorthPole { get; private set; }

        /// <summary>
        /// Südlichster Punkt der Region.
        /// </summary>
        public Point SouthPole { get; private set; }

        /// <summary>
        /// Id der Region (zur einfachen Identifikation).
        /// </summary>
        public string Id { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public double Orientation { get; set; }

        /// <summary>
        /// Liste aller Punkte der Region.
        /// </summary>
        public List<Point> Points { get; private set; }

        /// <summary>
        /// Informationen bezüglich der benachbarten Regionen.
        /// </summary>
        public int[] Environment { get; set; }

        /// <summary>
        /// Erzeugt eine neue Region anhand der übergebenen Eigenschaften.
        /// </summary>
        /// <param name="id">Id der Region.</param>
        /// <param name="center">Schwerpunt.</param>
        /// <param name="box">kleinstes umschließende Rechteck.</param>
        /// <param name="nrOfPoints">Anzahl der Punkte.</param>
        /// <param name="bsCenter">Mittelpunkt des umschließenden Kreises.
        /// </param>
        /// <param name="bsRad">Radius des umschließenden Kreises.</param>
        /// <param name="north">Nördlichster Punkt.</param>
        /// <param name="south">Südlichster Punkt.</param>
        /// <param name="histo"></param>
        /// <param name="points">Auflistung aller Punkte.</param>
        public Region(String id, Point center, Rectangle box, int nrOfPoints,
            Point bsCenter, int bsRad, Point north, Point south, double[] histo, 
            List<Point> points)
        {
            Id = id;
            CenterOfGravity = center;
            BoundingBox = box;
            NumberOfPoints = nrOfPoints;
            BoundingSphereCenter = bsCenter;
            BoundingSphereRadius = bsRad;
            NorthPole = north;
            SouthPole = south;
            Points = points;
        }
        
        /// <summary>
        /// Implementierung von IComparer zum sortieren der Regions nach dem
        /// Abstand zu einem bestimmten Punkt.
        /// </summary>
        public class DistanceComparer : IComparer<Region>
        {
            /// <summary>
            /// Punkt zu dem Der Abstand bestimmt werden soll.
            /// </summary>
            private Point p;

            /// <summary>
            /// Erstellt die Vergleichsklasse, die die Regions nach dem Abstand 
            /// zum Punkt p sortiert.
            /// </summary>
            /// <param name="p">Punkt zu dem Der Abstand bestimmt werden soll.
            /// </param>
            public DistanceComparer(Point p)
            {
                this.p = p;
            }

            /// <summary>
            /// Sortiert die Regionen.
            /// </summary>
            /// <param name="r1">erste Region.</param>
            /// <param name="r2">zweite Region.</param>
            /// <returns></returns>
            public int Compare(Region r1, Region r2)
            {
                double dist1 = ImageData.calculatePointDistance(
                       p, r1.CenterOfGravity);
                double dist2 = ImageData.calculatePointDistance(
                       p, r2.CenterOfGravity);

                if (dist1 < dist2)
                {
                    return -1;
                }
                else if (dist1 == dist2)
                {
                    return 0;
                }
                else
                {
                    return 1;
                }
            }
        }

        /// <summary>
        /// Implementierung von IComparable. Standardsortierung nach der größe
        /// des Radius des umschließenden Kreises
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            Region r = obj as Region;
            if (obj == null)
            {
                return -1;
            }
            else
            {
                int pointDiff = r.NumberOfPoints - NumberOfPoints;
                int radDiff = r.BoundingSphereRadius - BoundingSphereRadius;
                if (radDiff != 0)
                {
                    return radDiff;
                } 
                else if (pointDiff != 0) 
                {
                    return pointDiff;
                }
                else
                {
                    return r.BoundingBox.X * r.BoundingBox.Y
                        - BoundingBox.X * BoundingBox.Y;
                }
            }
        }

        /// <summary>
        /// Berechnet eine Abweichung der Eigenschaften zu den Eigenschaften
        /// einer übergebenen Region.
        /// </summary>
        /// <param name="r"></param>
        /// <param name="expactedLocation"></param>
        /// <param name="scale1"></param>
        /// <param name="scale2"></param>
        /// <param name="rotation"></param>
        /// <returns></returns>
        public double calculateDistance(Region r, Point expactedLocation, double scale1, double scale2, double rotation)
        {
            // diffrence between the estimated location of the cneter of gravity
            // and the real location in the given region r 
            double diffLocation = 0;
            if (expactedLocation.X > 0 && expactedLocation.Y > 0)
            {
                diffLocation = ImageData.calculatePointDistance(
                    r.CenterOfGravity, expactedLocation);
            }

            double diffRadius = Math.Abs(
                BoundingSphereRadius - scale1 * r.BoundingSphereRadius);
            double diffWeight = Math.Abs(
                NumberOfPoints - scale2 * r.NumberOfPoints);
            double diff1 = Math.Sqrt(Math.Pow(CenterOfGravity.X - BoundingSphereCenter.X, 2) +
                Math.Pow(CenterOfGravity.Y - BoundingSphereCenter.Y, 2));
            double diff2 = Math.Sqrt(Math.Pow(r.CenterOfGravity.X - r.BoundingSphereCenter.X, 2) +
                Math.Pow(r.CenterOfGravity.Y - r.BoundingSphereCenter.Y, 2));
            double res = diffRadius + diffWeight / 10 + Math.Abs(diff1 - diff2) * 10;

            double diffRotation = Math.Abs(Math.Abs(
                ImageData.calculateAngle(NorthPole,SouthPole) -
                ImageData.calculateAngle(r.NorthPole, r.SouthPole)) - 
                rotation);
            return diffLocation + diffRadius + diffWeight / 25;
        }

        public double compareEnvironment(Region r)
        {
            double diff = 0;
            for (int i = 0; i < 16; i++)
            {
                diff += Math.Abs(Environment[i] - r.Environment[i]);
            }
            return diff;
        }

        public override string  ToString()
        {
            return Id.ToString() + " " + CenterOfGravity.ToString();
        }

    }
}
