﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace matching
{
    /// <summary>
    /// Repräsentation der Bilddaten.
    /// </summary>
    class ImageData
    {
        /// <summary>
        /// Eine Auflistung der Bildregionen, absteigend sortiert nach der
        /// Anzahl der Punkte.
        /// </summary>
        public SortedSet<Region> Regions { get; private set; }

        /// <summary>
        /// Anzahl aller Punkte des Bildes.
        /// </summary>
        public int NumberOfPoints { get; private set; }

        /// <summary>
        /// Kleinstes Rechteck, das alle Punkte des Bildes enthält.
        /// </summary>
        public Rectangle BoundingBox { get; private set; }

        /// <summary>
        /// Erstellt eine neue Instanz einer Bildrepräsentation.
        /// </summary>
        /// <param name="regions">Auflistung der Regionen.</param>
        /// <param name="numberOfPoints">Anzahl der Bildpunkte.</param>
        /// <param name="boundingBox">umschließendes Rechteck.</param>
        public ImageData(SortedSet<Region> regions,
            int numberOfPoints, Rectangle boundingBox)
        {
            Regions = regions;
            NumberOfPoints = numberOfPoints;
            BoundingBox = boundingBox;

            addOrientationToRegions(200);
            addEnvironmentParametersToRegions(400);
        }

        /// <summary>
        /// Berechnet die n Regionen, die dem übergebenen Punkt p am nähesten 
        /// sind.
        /// </summary>
        /// <param name="n">Anzahl der Regionen, die zurückgegeben werden 
        /// sollen.</param>
        /// <param name="p">Bezugspunkt.</param>
        /// <returns>Die n nähesten Regionen sortiert nach der Entfernung zu p.
        /// </returns>
        public Region[] getNNearestRegions(int n, Point p)
        {
            SortedSet<Region> r = new SortedSet<Region>(
                                        Regions.AsEnumerable(),
                                        new Region.DistanceComparer(p));

            Region[] result = new Region[n];
            
            double dist = calculatePointDistance(
                r.ElementAt(0).CenterOfGravity,
                getNearestRegion(r.ElementAt(0)).CenterOfGravity);

            while (dist > 10 * r.ElementAt(0).BoundingSphereRadius)
            {
                r.Remove(r.First());
                dist = calculatePointDistance(
                    r.ElementAt(0).CenterOfGravity,
                    getNearestRegion(r.ElementAt(0)).CenterOfGravity);
            }
            for (int i = 0; i < n; i++)
            {
                result[i] = r.ElementAt(i);
            }
            return result;
        }

        /// <summary>
        /// Berechnet die Region, die der übergebenen Region am nähesten ist.
        /// </summary>
        /// <param name="region">Eine Bezugsregion.</param>
        /// <returns>Die näheste Region.</returns>
        public Region getNearestRegion(Region region)
        {
            SortedSet<Region> r = new SortedSet<Region>(
                                        Regions.AsEnumerable(),
                                        new Region.DistanceComparer(
                                            region.CenterOfGravity));
            r.Remove(region);
            return r.First();
        }

        /// <summary>
        /// Berechnet die Region, die dem übergebenen Punkt p am nähesten ist.
        /// </summary>
        /// <param name="p">Ein Bezugspunkt.</param>
        /// <returns>Die näheste Region.</returns>
        public Region getNearestRegion(Point p)
        {
            SortedSet<Region> r = new SortedSet<Region>(
                                        Regions.AsEnumerable(),
                                        new Region.DistanceComparer(p));
            return r.First();
        }

        #region "corners"
        /// <summary>
        /// Gibt die nordwestliche Ecke der BoundingBox zurück.
        /// </summary>
        /// <returns></returns>
        public Point getNorthWestCorner()
        {
            return new Point(
                        BoundingBox.Left,
                        BoundingBox.Top);
        }

        /// <summary>
        /// Gibt die nordöstliche Ecke der BoundingBox zurück.
        /// </summary>
        /// <returns></returns>
        public Point getNorthEastCorner()
        {
            return new Point(
                        BoundingBox.Left + BoundingBox.Width,
                        BoundingBox.Top);
        }

        /// <summary>
        /// Gibt die südöstliche Ecke der BoundingBox zurück.
        /// </summary>
        /// <returns></returns>
        public Point getSouthEastCorner()
        {
            return new Point(
                        BoundingBox.Left + BoundingBox.Width,
                        BoundingBox.Top - BoundingBox.Height);
        }

        /// <summary>
        /// Gibt die südwestliche Ecke der BoundingBox zurück.
        /// </summary>
        /// <returns></returns>
        public Point getSouthWestCorner()
        {
            return new Point(
                        BoundingBox.Left,
                        BoundingBox.Top - BoundingBox.Height);
        }
        #endregion

        /// <summary>
        /// Berechnet die Distanz zwischen zwei Punkten.
        /// </summary>
        /// <param name="p1">Erster Punkt.</param>
        /// <param name="p2">Zweiter Punkt.</param>
        /// <returns>Abstand der beiden Punkte.</returns>
        public static double calculatePointDistance(Point p1, Point p2)
        {
            return Math.Sqrt(Math.Pow((double)(p1.X - p2.X), 2)
                + Math.Pow((double)(p1.Y - p2.Y), 2));
        }

        /// <summary>
        /// Berechnet den Anstiegswinkel eines Vektors zwischen einem Anfangs-
        /// und einem Endpunkt.
        /// </summary>
        /// <param name="startpoint">Anfangspunkt.</param>
        /// <param name="endpoint">Endpunkt.</param>
        /// <returns>Anstiegswinkel im Intervall [0, 2*Pi)</returns>
        public static double calculateAngle(Point startpoint, Point endpoint)
        {
            double dX = (endpoint.X - startpoint.X);
            double dY = (startpoint.Y - endpoint.Y);

            if (dX == 0)
            {
                if (dY > 0) 
                { 
                    return Math.PI * 0.5;
                } 
                else if (dY < 0) 
                {
                    return Math.PI * 1.5;
                } 
                else 
                {
                    return 0d;
                }
            }
            else
            {
                double angle = Math.Atan(dY / dX);
                if (dX < 0) angle += Math.PI;
                return (angle + 2 * Math.PI) % (2 * Math.PI);
            }
        }

        /// <summary>
        /// Gibt eine Liste aller Punkte des Bildes zurück.
        /// </summary>
        /// <returns>Liste mit Punkten.</Point></returns>
        public List<Point> getAllPoints()
        {
            List<Point> points = new List<Point>();
            foreach (Region r in Regions)
                points.AddRange(r.Points);
            return points;
        }

        /// <summary>
        /// Gibt alle Punkte innerhalb eines angegebenen Radius von einem 
        /// Bezugspunkt zurück.
        /// </summary>
        /// <param name="center">Bezugspunkt.</param>
        /// <param name="range">Radius.</param>
        /// <returns>Liste der Punkte.</returns>
        public List<Point> getAllPointsInRadius(Point center, double radius)
        {
            List<Point> points = getAllPoints();
            List<Point> result = new List<Point>();
            foreach (Point p in points)
                if (calculatePointDistance(p, center) <= radius)
                    result.Add(p);
            return result;
        }

        /// <summary>
        /// Berechnet den Schwerpunkt einer Menge von Punkten.
        /// </summary>
        /// <param name="points">Liste von Punkten.</param>
        /// <returns>Schwerpunkt.</returns>
        public Point getCenterOfGravity(List<Point> points)
        {
            int x = 0;
            int y = 0;
            foreach (Point p in points)
            {
                x += p.X;
                y += p.Y;
            }
            return new Point(x / points.Count, y / points.Count);
        }

        /// <summary>
        /// Fügt den Regionen einen Orientierungswinkel hinzu.
        /// </summary>
        /// <param name="radius">Zur Berechung des Orientierungswinkels werden
        /// alle Punkte innerhalb des angegebene Radius (ausgehend vom
        /// Schwerpunkt der Region) beachtet.</param>
        private void addOrientationToRegions(double radius)
        {
            foreach (Region r in Regions)
            {
                List<Point> points = getAllPointsInRadius(
                    r.CenterOfGravity, radius);
                Point p = getCenterOfGravity(points);
                r.Orientation = calculateAngle(r.CenterOfGravity, p);
            }
        }

        /// <summary>
        /// Fügt den Regionen Informationen bezüglich der Umgebung hinzu.
        /// </summary>
        /// <param name="width">Breite des Umgebungsbereichs.</param>
        private void addEnvironmentParametersToRegions(double width)
        {
            List<Point> regionPoints = new List<Point>();
            foreach (Region r in Regions)
                regionPoints.Add(r.CenterOfGravity);

            foreach (Region r in Regions)
            {
                int[] environment = new int[16];
                double sin = Math.Sin(r.Orientation);
                double cos = Math.Cos(r.Orientation);
                List<Point> points = new List<Point>();
                foreach (Point p in regionPoints)
                {
                    double x = cos * (p.X - r.CenterOfGravity.X)
                               - sin * (p.Y - r.CenterOfGravity.Y);
                    double y = sin * (p.X - r.CenterOfGravity.X)
                               + cos * (p.Y - r.CenterOfGravity.Y);

                    int ix = -1;
                    int iy = -1;
                    double x0 =  - 0.5 * width;
                    double y0 =  - 0.5 * width;

                    for (int i = 0; i < 5; i++)
                    {
                        if (x > x0) ix++;
                        if (y > y0) iy++;
                        x0 += 0.25 * width;
                        y0 += 0.25 * width;
                    }
                    if (ix >= 0 && ix < 4 && iy >= 0 && iy < 4)
                        environment[ix + 4 * iy] += 1;
                }
                r.Environment = environment;                
            }
        }
    }
}
