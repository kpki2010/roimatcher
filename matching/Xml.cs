﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Xml;

namespace matching
{
    /// <summary>
    /// Diese Klasse stellt Funktionen zum Lesen und Schreiben von XML-Dateien
    /// bereit.
    /// </summary>
    class Xml
    {
        /// <summary>
        /// Liest eine XML-Datei(Region-Repräsentation) ein und erstellt ein 
        /// ImageData-Objekt.
        /// </summary>
        /// <param name="xmlFileName">Der Speicherort der XML-Datei.</param>
        /// <returns>Ein ImageDatat-Objekt.</returns>
        public static ImageData getRegionsFromXml(String xmlFileName)
        {
            SortedSet<Region> regions = new SortedSet<Region>();

            XmlTextReader xmlReader = new XmlTextReader(xmlFileName);

            Point centerOfGravity = new Point(0, 0);
            Rectangle boundingBox = new Rectangle(0, 0, 0, 0);
            Point centerOfBoundingSphere = new Point(0, 0);
            int boundingSphereRad = 0;
            int numberOfPoints = 0;
            Point northPole = new Point(0, 0);
            Point southPole = new Point(0, 0);
            string id = "";
            double[] histo = new double[5];
            double a = 0;
            double step = 0;
            int totalPoints = 0;

            int nPole = -1;
            int sPole = 0;
            int ePole = 0;
            int wPole = -1;

            List<Point> points = new List<Point>();

            while (xmlReader.Read())
            {

                if (xmlReader.Name.Equals("region")
                  && xmlReader.NodeType.Equals(XmlNodeType.Element))
                {
                    id = xmlReader.GetAttribute("id");
                }
                else if (xmlReader.Name.Equals("boundingbox"))
                {
                    boundingBox = new Rectangle(
                        Int32.Parse(xmlReader.GetAttribute("x")),
                        Int32.Parse(xmlReader.GetAttribute("y")),
                        Int32.Parse(xmlReader.GetAttribute("width")),
                        Int32.Parse(xmlReader.GetAttribute("height")));
                }
                else if (xmlReader.Name.Equals("centerofgravity"))
                {
                    centerOfGravity = new Point(
                        Int32.Parse(xmlReader.GetAttribute("x")),
                        Int32.Parse(xmlReader.GetAttribute("y")));
                }
                else if (xmlReader.Name.Equals("boundingsphere"))
                {
                    centerOfBoundingSphere = new Point(
                        Int32.Parse(xmlReader.GetAttribute("x")),
                        Int32.Parse(xmlReader.GetAttribute("y")));
                    boundingSphereRad = Int32.Parse(
                        xmlReader.GetAttribute("radius"));
                }
                else if (xmlReader.Name.Equals("northpole"))
                {
                    northPole = new Point(
                        Int32.Parse(xmlReader.GetAttribute("x")),
                        Int32.Parse(xmlReader.GetAttribute("y")));
                }
                else if (xmlReader.Name.Equals("southpole"))
                {
                    southPole = new Point(
                        Int32.Parse(xmlReader.GetAttribute("x")),
                        Int32.Parse(xmlReader.GetAttribute("y")));

                    a = ImageData.calculateAngle(northPole, southPole);
                    step = ImageData.calculatePointDistance(northPole, southPole) / 5d;

                }
                else if (xmlReader.Name.Equals("pixel"))
                {
                    int x = Int32.Parse(xmlReader.GetAttribute("x"));
                    int y = Int32.Parse(xmlReader.GetAttribute("y"));
                    points.Add(new Point(x, y));
                    
                    if (y < nPole || nPole < 0) nPole = y;
                    if (y > sPole) sPole = y;
                    if (x > ePole) ePole = x;
                    if (x < wPole || wPole < 0) wPole = x;
                    numberOfPoints++;
                }
                else if (xmlReader.Name.Equals("region")
                  && xmlReader.NodeType.Equals(XmlNodeType.EndElement))
                {
                    if (numberOfPoints > 10)
                    {
                        for (int i = 0; i < 5; i++)
                        {
                            //histo[i] = histo[i] / numberOfPoints;
                            regions.Add(new Region(id, centerOfGravity, boundingBox,
                                numberOfPoints, centerOfBoundingSphere,
                                boundingSphereRad, northPole, southPole, histo, points));
                        }
                        totalPoints += numberOfPoints;
                    }
                    numberOfPoints = 0;
                    points = new List<Point>();
                    histo = new double[5];
                }
            }

            return new ImageData(regions, totalPoints,
                new Rectangle(wPole, nPole, ePole - wPole, nPole - sPole));
        }

        /// <summary>
        /// Schreibt den Inhalt des MatchingResult-Objekts in eine XMl-Datei.
        /// </summary>
        /// <param name="mResult">Das MatchingResult-Objekt.</param>
        /// <param name="output">Der Speicherort für die Datei.</param>
        public static void writeResultToXml(MatchingResult mResult,
            String output)
        {
            XmlTextWriter xmlWriter = new XmlTextWriter(
                output, System.Text.Encoding.UTF8);
            xmlWriter.Formatting = Formatting.Indented;

            xmlWriter.WriteStartElement("MatchingResult");
            xmlWriter.WriteElementString(
                "deviation", mResult.Deviation.ToString("0.0000"));
            xmlWriter.WriteStartElement("homography");
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    xmlWriter.WriteStartElement("at");
                    xmlWriter.WriteAttributeString("i", (i+1).ToString());
                    xmlWriter.WriteAttributeString("j", (j+1).ToString());
                    xmlWriter.WriteString(
                        mResult.Homography.get(i, j).ToString("0.0000"));
                    xmlWriter.WriteEndElement();
                }
            }
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("mapping");

            foreach (KeyValuePair<Region, Region> pair in mResult.Mapping)
            {
                xmlWriter.WriteStartElement("pair");
                xmlWriter.WriteElementString("region1", pair.Key.Id);
                xmlWriter.WriteElementString("region2", pair.Value.Id);
                xmlWriter.WriteEndElement();
            }
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndElement();
            xmlWriter.Flush();
            xmlWriter.Close();
        }
    }
}
