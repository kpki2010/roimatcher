﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Drawing.Imaging;

namespace matching
{
    public partial class gui : Form
    {
        String image1 = @"image1.bmp";
        String xml1 = @"data1.xml";
        String image2 = @"image2.png";
        String xml2 = @"data2.xml";

        String imageFile1 = @"image1.bmp";
        String xmlFile1 = @"data1.xml";
        String imageFile2 = @"image2.png";
        String xmlFile2 = @"data2.xml";

        private Homography homo;

        public gui()
        {
            InitializeComponent(); 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            imageFile1 = openFileDialog1.FileName;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            imageFile1 = textBox2.Text + image1;
            imageFile2 = textBox2.Text + image2;
            xmlFile1 = textBox2.Text + xml1;
            xmlFile2 = textBox2.Text + xml2;


            for (int i = 0; i < 1; i++)
            {
                pictureBox1.Image = Image.FromFile(imageFile1);
                pictureBox2.Image = Image.FromFile(imageFile2);
                Graphics g1 = Graphics.FromImage(pictureBox1.Image);
                Graphics g2 = Graphics.FromImage(pictureBox2.Image);
                
                ImageData d1 = Xml.getRegionsFromXml(xmlFile1);
                ImageData d2 = Xml.getRegionsFromXml(xmlFile2);

                MatchingResult res = Matching.match(xmlFile1, xmlFile2, textBox2.Text + "result.xml", (int)numericUpDown1.Value);

                homo = res.Homography;
                textBox1.Text = homo.ToString();

                listBox1.Items.Clear();
                foreach (KeyValuePair<Region, Region> pair in res.Mapping)
                {
                    listBox1.Items.Add(pair);
                }

                listBox2.Items.Clear();
                foreach (Region r in d1.Regions)
                {
                    listBox2.Items.Add(r);
                }

                listBox3.Items.Clear();
                foreach (Region r in d2.Regions)
                {
                    listBox3.Items.Add(r);
                }

                textBox1.Text += "\r\n total: " + listBox1.Items.Count;


                foreach (Region r in d1.Regions)
                {
                    foreach (Point p in r.Points)
                    {
                        Point p1 = homo.getMappedPoint(p);
                        g2.DrawRectangle(Pens.LightGreen, p1.X, p1.Y, 1, 1);
                    }
                }

                pictureBox2.Image.Save(textBox3.Text + "result" + i.ToString() + ".bmp");
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            KeyValuePair<Region, Region> pair = (KeyValuePair<Region, Region>)
                listBox1.Items[listBox1.SelectedIndex];

            pictureBox1.Image = Image.FromFile(imageFile1);
            pictureBox2.Image = Image.FromFile(imageFile2);

            Graphics g1 = Graphics.FromImage(pictureBox1.Image);
            Graphics g2 = Graphics.FromImage(pictureBox2.Image);

            drawRegion(pair.Key, g1);
            drawRegion(pair.Value, g2);

            foreach (Point p0 in pair.Key.Points)
            {
                Point p1 = homo.getMappedPoint(p0);
                g2.DrawRectangle(Pens.LightCyan, p1.X, p1.Y, 1, 1);
            }

            Point p = homo.getMappedPoint(pair.Key.CenterOfGravity);
            g2.FillEllipse(Brushes.Red, p.X - 2, p.Y - 2, 5, 5);
            Refresh();
        }

        private void drawRegion(Region r, Graphics g)
        {
            g.DrawRectangle(Pens.Red, r.BoundingBox);

            double a = Math.Tan(r.Orientation);
            Point p = new Point(
                r.CenterOfGravity.X + (int)(Math.Cos(r.Orientation) * 100),
                r.CenterOfGravity.Y - (int)(Math.Sin(r.Orientation) * 100));

            g.DrawLine(Pens.Green, r.CenterOfGravity, p);
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Region r = (Region)listBox2.SelectedItem;
            pictureBox1.Image = Image.FromFile(imageFile1);
            Graphics g1 = Graphics.FromImage(pictureBox1.Image);
            drawRegion(r, g1);
            ImageData d1 = Xml.getRegionsFromXml(xmlFile1);


            List<Point> pnts = new List<Point>();
            foreach (Region regs in d1.Regions)
                pnts.Add(regs.CenterOfGravity);

            double sin = Math.Sin(r.Orientation);
            double cos = Math.Cos(r.Orientation);
            int[] environment = new int[16];
            int width = 400;
            foreach (Point p in pnts)
            {
                double x = cos * (p.X - r.CenterOfGravity.X)
                           - sin * (p.Y - r.CenterOfGravity.Y);
                double y = sin * (p.X - r.CenterOfGravity.X)
                           + cos * (p.Y - r.CenterOfGravity.Y);
                g1.FillRectangle(Brushes.LightGreen, (int)x + r.CenterOfGravity.X - 1,
                    r.CenterOfGravity.Y - 1 + (int)y, 3, 3);

                int ix = -1;
                int iy = -1;
                double x0 = -0.5 * width;
                double y0 = -0.5 * width;

                for (int i = 0; i < 5; i++)
                {
                    if (x > x0) ix++;
                    if (y > y0) iy++;
                    x0 += 0.25 * width;
                    y0 += 0.25 * width;
                }
                if (ix >= 0 && ix < 4 && iy >= 0 && iy < 4)
                    environment[ix + 4 * iy] += 1;

            }
            r.Environment = environment;

            int yi = r.BoundingSphereCenter.Y - 200;
            for (int i = 0; i < 4; i++)
            {
                int xi = r.BoundingSphereCenter.X - 200;
                for (int j = 0; j < 4; j++)
                {
                    g1.DrawRectangle(Pens.Aqua, xi, yi, 100, 100);
                    xi += 100;
                }
                yi += 100;
            }
            foreach (Region reg in d1.Regions)
                g1.DrawRectangle(Pens.Red, reg.BoundingSphereCenter.X,
                    reg.BoundingSphereCenter.Y, 1, 1);
            Refresh();
            textBox5.Text = "";
            for (int i = 0; i < 16; i++)
            {
                textBox5.Text += r.Environment[i].ToString() + " ";
            }
        }

        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            Region r = (Region)listBox3.SelectedItem;
            pictureBox2.Image = Image.FromFile(imageFile2);
            Graphics g2 = Graphics.FromImage(pictureBox2.Image);
            drawRegion(r, g2);
            ImageData d2 = Xml.getRegionsFromXml(xmlFile2);


            List<Point> pnts = new List<Point>();
            foreach (Region regs in d2.Regions)
                pnts.Add(regs.CenterOfGravity);

            double sin = Math.Sin(r.Orientation);
            double cos = Math.Cos(r.Orientation);
            int[] environment = new int[16];
            int width = 400;
            foreach (Point p in pnts)
            {
                double x = cos * (p.X - r.CenterOfGravity.X)
                           - sin * (p.Y - r.CenterOfGravity.Y);
                double y = sin * (p.X - r.CenterOfGravity.X)
                           + cos * (p.Y - r.CenterOfGravity.Y);
                g2.FillRectangle(Brushes.LightGreen, (int)x + r.CenterOfGravity.X - 1,
                    r.CenterOfGravity.Y - 1 + (int)y, 3, 3);

                int ix = -1;
                int iy = -1;
                double x0 = -0.5 * width;
                double y0 = -0.5 * width;

                for (int i = 0; i < 5; i++)
                {
                    if (x > x0) ix++;
                    if (y > y0) iy++;
                    x0 += 0.25 * width;
                    y0 += 0.25 * width;
                }
                if (ix >= 0 && ix < 4 && iy >= 0 && iy < 4)
                    environment[ix + 4 * iy] += 1;

            }
            r.Environment = environment;

            int yi = r.BoundingSphereCenter.Y - 200;
            for (int i = 0; i < 4; i++)
            {
                int xi = r.BoundingSphereCenter.X - 200;
                for (int j = 0; j < 4; j++)
                {
                    g2.DrawRectangle(Pens.Aqua, xi, yi, 100, 100);
                    xi += 100;
                }
                yi += 100;
            }
            foreach (Region reg in d2.Regions)
                g2.DrawRectangle(Pens.Red, reg.BoundingSphereCenter.X,
                    reg.BoundingSphereCenter.Y, 1, 1);
            Refresh();
            textBox4.Text = "";
            for (int i = 0; i < 16; i++)
            {
                textBox4.Text += r.Environment[i].ToString() + " ";
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            int[] diff = new int[16];
            string[] str1 = textBox4.Text.Split(Convert.ToChar(" "));
            string[] str2 = textBox5.Text.Split(Convert.ToChar(" "));

            textBox6.Text = "";
            int count = 0;
            for (int i = 0; i < 16; i++)
            {
                int x = Math.Abs(Int32.Parse(str1[i]) - Int32.Parse(str2[i]));
                textBox6.Text += x.ToString() + " ";
                count += x;
            }
            textBox6.Text += "\r\n" + count.ToString();
        }


    }
}
