﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Qoniac.Common.MathHelpers;

namespace matching
{
    /// <summary>
    /// Eine 3x3-Transformationsmatrix zur Berechnung der Bildtransformation.
    /// </summary>
    class Homography : Matrix
    {
        /// <summary>
        /// Erstellt eine neue Transformationsmatrix und füllt diese.
        /// </summary>
        /// <param name="matrix">Parameter mit der die Matrix gefüllt werden 
        /// soll.</param>
        public Homography(double[] matrix) : base(matrix, 3, 3) { }

        public static Homography estimateFromPoints(
            KeyValuePair<Point, Point>[] pairs)
        {
            LinearEquationSystem les = constructLES(pairs);
            
            les.SolveNoDestroy();
            double[] x = new double[9];
            for (int i = 0; i < 8; i++)
                x[i] = les.Ergs[i];
            x[8] = 1;
            return new Homography(x);
        }

        /// <summary>
        /// Erstellt ein lineares Gleichungssystem und füllt dieses mit den
        /// Parametern, die aus den Punktpaaren errechnet werden.
        /// </summary>
        /// <param name="pairs">Paare von Punktkorrespondenzen.</param>
        /// <returns>Ein lineares Gleichungssystem.</returns>
        private static LinearEquationSystem constructLES(
            KeyValuePair<Point, Point>[] pairs)
        {
            LinearEquationSystem les = new LinearEquationSystem(
                                                9, pairs.Length * 2);

            for (int i = 0; i < pairs.Length; i++)
            {
                double[] src = getHomogeneosCoordinate(pairs[i].Key);
                double[] tar = getHomogeneosCoordinate(pairs[i].Value);

                double[] Ax = getAx(src, tar);
                double[] Ay = getAy(src, tar);

                for (int j = 0; j < 9; j++)
                {
                    les[i * 2, j] = Ax[j];
                    les[i * 2 + 1, j] = Ay[j];
                }
            }
            return les;
        }

        /// <summary>
        /// Erstellt ein Punkt im homogenen Koordinatensystem von einem 
        /// Punkt p.
        /// </summary>
        /// <param name="p">Punkt, dessen homogener Punkt errechnet werden 
        /// soll.</param>
        /// <returns>Ein homogenr Punkt als Array</returns>
        private static double[] getHomogeneosCoordinate(Point p)
        {
            return new double[] { (double)p.X, (double)p.Y, 1 };
        }

        /// <summary>
        /// Wandelt einen 3-dimensionalen homogenen Punkt in einen 
        /// 2-dimensionale kartesischen Punkt um.
        /// </summary>
        /// <param name="p">Koordinate, die umgewandelt werden soll.</param>
        /// <returns>Der Umgewandelte Punkt oder der Punkt (-1, -1) falls eine 
        /// Koordinate des berechnete Punkt außerhalb der Dimension eines 
        /// Integers liegt.</returns>
        private static Point get2DCoordinate(double[] p)
        {
            try
            {
                return new Point(
                    Convert.ToInt32(p[0] / p[2]),
                    Convert.ToInt32(p[1] / p[2]));
            }
            catch
            {
                return new Point(-1, -1);
            }
        }

        /// <summary>
        /// Erstellt eine der Zeile des Gleichungssystems zum Errechnen der
        /// Transformationsmatrix anhand einer Punktkorrespondenz.
        /// </summary>
        /// <param name="src">Der Ausgangsunkt.</param>
        /// <param name="tar">Der Punkt auf den der Ausgangspunkt abgebildet 
        /// werden soll.</param>
        /// <returns>Ein Zeile des Gleichnugssystems.</returns>
        private static double[] getAx(double[] src, double[] tar)
        {
            return new double[] { tar[0], -src[0], -src[1], -1, 0, 0, 0,
                tar[0]*src[0], tar[0]*src[1] };
        }

        /// <summary>
        /// Analog getAx. Erstellt die zweite Zeile des Gleichungssystems zum 
        /// Errechnen der Transformationsmatrix anhand einer Punktkorrespondenz.
        /// </summary>
        /// <param name="src">Der Ausgangspunkt.</param>
        /// <param name="tar">Der Punkt auf den der Ausgangspunkt abgebildet 
        /// werden soll.</param>
        /// <returns>Ein Zeile des Gleichnugssystems.</returns>
        private static double[] getAy(double[] src, double[] tar)
        {
            return new double[] { tar[1], 0, 0, 0, -src[0], -src[1], -1, 
                tar[1]*src[0], tar[1]*src[1] };
        }

        /// <summary>
        /// Errechnet den Punkt, auf den der Ausgangspunkt mit Hilfe der Matrix
        /// abgebildet wird.
        /// </summary>
        /// <param name="src">Ein Ausgangspunkt.</param>
        /// <returns>Das Bild des Ausgangspunktes.</returns>
        public Point getMappedPoint(Point src)
        {
            Matrix x = new Matrix(getHomogeneosCoordinate(src), 3, 1);
            Matrix m = multiply(this, x);
            return get2DCoordinate(m.getColumn(0));
        }
    }
}
