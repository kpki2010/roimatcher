﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace matching
{
    /// <summary>
    /// Klasse zur Verarbeitung von Konsolen-Aufrufen.
    /// </summary>
    class LibConsole
    {
        /// <summary>
        /// Liest die Argumente ein und ordnet den Parametern (Elemente, die mit
        ///  "--" beginnen) eine Liste ihrer Werte zu (alle folgenden Elemente 
        ///  bis zum nächsten Parameter).
        /// </summary>
        /// <param name="args">Ein String-Arrary, der die Argumente des Aufrufs 
        /// beinhaltet.</param>
        /// <returns>Eine Auflistung der Parameter und der dazugehörigen Werte.
        /// </returns>
        public static Dictionary<string, List<string>> parseParameters(
            String[] args)
        {
            Dictionary<string, List<string>> parameters = 
                new Dictionary<string,List<string>>();

            int i = 0;
            while (i < args.Length)
            {
                string key = args[i];
                if (key.StartsWith("--"))
                {
                    key = key.Replace("--", "");
                    List<string> param = new List<string>();

                    i++;
                    while (i < args.Length && !args[i].StartsWith("--")) 
                    {
                        param.Add(args[i]);
                        i++;
                    }
                    i--;
                    parameters.Add(key, param);
                }
                i++;
            }
            return parameters;
        }
    }
}
