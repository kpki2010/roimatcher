﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace matching
{
    /// <summary>
    /// Eine Matrix.
    /// </summary>
    class Matrix
    {
        /// <summary>
        /// Interne Repräsentation der Matrixparameter.
        /// </summary>
        protected double[,] matrix;

        /// <summary>
        /// Anzahl der Zeilen.
        /// </summary>
        public int NumberOfRows { get; private set; }

        /// <summary>
        /// Anzahl der Spalten.
        /// </summary>
        public int NumberOfColumns { get; private set; }

        #region "constructor"
        /// <summary>
        /// Erstellt eine neue Matrix ohne Inhalt.
        /// </summary>
        /// <param name="rows">Anzahl der Zeilen.</param>
        /// <param name="columns">Anzahl der Spalten.</param>
        public Matrix(int rows, int columns)
        {
            matrix = new double[rows, columns];
            NumberOfRows = rows;
            NumberOfColumns = columns;
        }

        /// <summary>
        /// Erstellt eine neue Matrix und füllt diese.
        /// </summary>
        /// <param name="values">2-dimensionaler-Array mit den Matrixparametern.
        /// </param>
        /// <param name="rows">Anzahl der Zeilen.</param>
        /// <param name="columns">Anzahl der Spalten.</param>
        public Matrix(double[,] values, int rows, int columns)
        {
            matrix = values;
            NumberOfRows = rows;
            NumberOfColumns = columns;
        }

        /// <summary>
        /// Erstellt eine neue Matrix und füllt diese.
        /// </summary>
        /// <param name="values">Matrixparameter zeilenweise hintereinander in
        /// einem Array.</param>
        /// <param name="rows">Anzahl der Zeilen.</param>
        /// <param name="columns">Anzahl der Spalten.</param>
        public Matrix(double[] values, int rows, int columns)
        {
            matrix = new double[rows, columns];
            int row = 0;
            int column = 0;
            for (int i = 0; i < values.Length; i++)
            {
                matrix[row, column] = values[i];
                if (column < columns - 1)
                {
                    column++;
                }
                else if (row < rows - 1)
                {
                    column = 0;
                    row++;
                }
            }
            NumberOfRows = rows;
            NumberOfColumns = columns;
        }
        #endregion

        /// <summary>
        /// Gibt den Parameter der Matrix an angegebener Position zurück.
        /// </summary>
        /// <param name="row">Zeile in der der Paramater steht. (Zählung beginnt
        /// bei 0)</param>
        /// <param name="column">Spalte in der der Paramater steht. (Zählung 
        /// beginnt bei 0)</param>
        /// <returns>Wert des Parameters.</returns>
        #region "getter/setter"
        public double get(int row, int column)
        {
            if (row < NumberOfRows && column < NumberOfColumns && row >= 0 && column >= 0) 
            {
                return matrix[row, column]; 
            } 
            else 
            {
                return 0;
            }
        }

        /// <summary>
        /// Setzt den Wert eines Parameters.
        /// </summary>
        /// <param name="row">Zeile des Paramaters.</param>
        /// <param name="column">Spalte des Paramaters.</param>
        /// <param name="value">Wert des Parameters.</param>
        public void set(int row, int column, double value)
        {
            if (row < NumberOfRows && column < NumberOfColumns && row >= 0 && column >= 0)
            {
                matrix[row, column] = value;
            }
        }

        /// <summary>
        /// Gibt eine Zeile der Matrix zurück.
        /// </summary>
        /// <param name="index">Index der Zeile.</param>
        /// <returns>Die Zeile als Array.</returns>
        public double[] getRow(int index)
        {
            if (index >= 0 && index < NumberOfRows)
            {

                double[] row = new double[NumberOfColumns];
                for (int i = 0; i < NumberOfColumns; i++)
                {
                    row[i] = matrix[index, i];
                }
                return row;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gibt eine Spalte der Matrix zurück.
        /// </summary>
        /// <param name="index">Index der Spalte.</param>
        /// <returns>Die Spalte als Array.</returns>
        public double[] getColumn(int index)
        {
            if (index >= 0 && index < NumberOfColumns)
            {

                double[] column = new double[NumberOfRows];
                for (int i = 0; i < NumberOfRows; i++)
                {
                    column[i] = matrix[i, index];
                }
                return column;
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region "class methods"
        /// <summary>
        /// Multipliziert 2 Matrizen miteinander und gibt das Resultat als neue
        /// Matrix zurück.
        /// </summary>
        /// <param name="a">erste Matrix.</param>
        /// <param name="b">zweite Matrix</param>
        /// <returns>Neue Matrix, die das Resultat der Multiplikation enthält.
        /// </returns>
        public static Matrix multiply(Matrix a, Matrix b)
        {
            if (a.NumberOfColumns == b.NumberOfRows)
            {
                Matrix c = new Matrix(a.NumberOfRows, b.NumberOfColumns);


                for (int i = 0; i < c.NumberOfRows; i++)
                {
                    for (int j = 0; j < c.NumberOfColumns; j++)
                    {
                        double sum = 0;
                        for (int k = 0; k < a.NumberOfColumns; k++)
                        {
                            sum += a.get(i, k) * b.get(k, j);
                        }
                        c.set(i, j, sum);
                    }
                }
                return c;
            }
            else
            {
                return null;
            }
        }

        #endregion

        #region "individual methods"
        /// <summary>
        /// Transponiert diese Matrix und gibt das Resultat als neue Matrix 
        /// zurück.
        /// </summary>
        /// <returns>Neue Matrix mit dem Resultat.</returns>
        public Matrix transpose()
        {
            Matrix m = new Matrix(NumberOfColumns, NumberOfRows);
            for (int i = 0; i < NumberOfRows; i++)
            {
                for (int j = 0; j < NumberOfColumns; j++)
                {
                    m.set(j + 1, i + 1, matrix[i, j]);
                }
            }
            return m;
        }

        /// <summary>
        /// Erstellt eine Zeichenkettenrepräsentation der Matrix.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            String s = "";
            for (int i = 0; i < NumberOfRows; i++)
            {
                for (int j = 0; j < NumberOfColumns; j++)
                {
                    s += matrix[i, j].ToString("0.000") + "\t";
                }
                s += "\r\n";
            }
            return s;
        }
        #endregion
    }
}