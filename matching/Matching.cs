﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Xml;


namespace matching
{
    /// <summary>
    /// Die Klasse stellt Funktionen zum Berechnen des Matchings bereit.
    /// </summary>
    class Matching
    {
        /// <summary>
        /// Liest die Quelldateien ein (zwei Xml-Dateien, die jeweils eine
        /// Regionenbeschreibung eines Bildes repräsentieren), berechnet ein 
        /// Matching und schreibt das Ergebnis in eine Xml-Datei.
        /// </summary>
        /// <param name="file1">Erste Quelldatei.</param>
        /// <param name="file2">Zweite Quelldatei.</param>
        /// <param name="output">Dateiname für die Ergebnisdatei.</param>
        /// <param name="numberOfIterations">Anzahl der Wiederholungen bei dem
        /// RANSAC-Verfahren.</param>
        /// <returns>Gibt das Ergebnis des Matchings zurück.</returns>
        public static MatchingResult match(
            string file1, string file2, string output, int numberOfIterations)
        {
            ImageData source = Xml.getRegionsFromXml(file1);
            ImageData target = Xml.getRegionsFromXml(file2);

            MatchingResult r = matchRegions(source, target, numberOfIterations);
            Xml.writeResultToXml(r, output);
            return r;
        }

        private static MatchingResult matchRegions(ImageData sourceData,
            ImageData targetData, int numberOfIterations)
        {
            SortedSet<Region> source = sourceData.Regions;
            SortedSet<Region> target = targetData.Regions;
            List<KeyValuePair<Region, Region>> assignments =
                new List<KeyValuePair<Region, Region>>();

            List<Region> assignedRegions = new List<Region>();

            for (int i = 0; i < source.Count; i++)
            {
                Region rSrc = source.ElementAt(i);

                Region nn = null;

                double dist = -1;
                for (int j = 0; j < target.Count; j++)
                {
                    Region rTar = target.ElementAt(j);

                    double d = rSrc.compareEnvironment(rTar);
                    if (assignedRegions.Contains(rTar))
                        d = d * 3;
                    if (dist == -1 || d < dist)
                    {
                        dist = d;
                        nn = rTar;
                    }

                }
                assignments.Add(new KeyValuePair<Region, Region>(
                        rSrc, nn));
                assignedRegions.Add(nn);
            }

            return ransac(assignments, sourceData, targetData, numberOfIterations);
        }
        
        /// <summary>
        /// RANSAC-Verfahren zum Auswerten des Matchings und Erstellen des 
        /// Ergebnis-Objekts.
        /// </summary>
        /// <param name="assignment">Die Zuordnungen der Regionen als Liste von
        /// KeyValuePairs.</param>
        /// <param name="source">Die Bildinformationen des Ausgangsbildes (erste
        /// Quelldatei).</param>
        /// <param name="target">Die Bildinformationen des Zielbildes (zweite
        /// Quelldatei).</param>
        /// <param name="numberOfInteration">Anzahl der Wiederholungen beim
        /// RANSAC-Verfahren.</param>
        /// <returns>Ein MatchingResult-Objekt.</returns>
        public static MatchingResult ransac(
            List<KeyValuePair<Region, Region>> assignment,
            ImageData source, 
            ImageData target, 
            int numberOfInteration)
        {

            double deviation = -1d;
            double totaldev = 0d;

            Random rand = new Random();

            List<KeyValuePair<Region, Region>> bestMapping = null;
            Homography bestHomography = null;

            for (int i = 0; i < numberOfInteration; i++)
            {
                double dev1 = 0d;

                double dev = 0d;
                int count = 0;

                KeyValuePair<Point, Point>[] map =
                    new KeyValuePair<Point, Point>[4];

                for (int j = 0; j < 4; j++)
                {
                    int idx = rand.Next(assignment.Count);
                    map[j] = new KeyValuePair<Point, Point>(
                        assignment[idx].Key.CenterOfGravity,
                        assignment[idx].Value.CenterOfGravity);
                }
                Homography homography = Homography.estimateFromPoints(map);

                List<KeyValuePair<Region, Region>> rightMapped =
                    new List<KeyValuePair<matching.Region, matching.Region>>();

                List<Region> mappedRegionsTar = new List<Region>();
                foreach (KeyValuePair<Region, Region> pair in assignment)
                {
                    double dist = evaluatePointMapping(homography, pair);

                    if (dist < 15 + pair.Value.BoundingSphereRadius
                        && !mappedRegionsTar.Contains(pair.Value))
                    {
                        rightMapped.Add(pair);
                        mappedRegionsTar.Add(pair.Value);
                        dev += dist * 0.5 * pair.Value.NumberOfPoints;
                        count++;
                    }
                    else
                    {
                        dev1++;
                    }
                }

                if (bestMapping == null || rightMapped.Count > bestMapping.Count)
                {
                    bestMapping = rightMapped;
                    bestHomography = homography;
                    deviation = dev1 / (double)source.NumberOfPoints *
                        (double)Math.Abs(
                        source.NumberOfPoints - target.NumberOfPoints);

                    totaldev = dev;
                }
                else if (rightMapped.Count == bestMapping.Count)
                {

                    if (totaldev > dev)
                    {
                        bestMapping = rightMapped;
                        bestHomography = homography;

                        deviation = dev1 / (double)source.NumberOfPoints *
                            (double)Math.Abs(
                            source.NumberOfPoints - target.NumberOfPoints);

                        totaldev = dev;
                    }
                }
            }
            return new MatchingResult(bestMapping, bestHomography, deviation);
        }

        private static double evaluatePointMapping(Homography homography,
            KeyValuePair<Region, Region> p)
        {
            return ImageData.calculatePointDistance(
                        homography.getMappedPoint(p.Key.CenterOfGravity), 
                        p.Value.CenterOfGravity);
        }

        
    }
}
