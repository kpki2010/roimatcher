﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace matching
{
    static class Program
    {
        /// <summary>
        /// Liest die Parameter ein und startet das Programm.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                runGUI();
            }
            else
            {
                Dictionary<string, List<string>> param = 
                    LibConsole.parseParameters(args);

                if (param.ContainsKey("help"))
                {
                    System.Console.WriteLine("Please use the following form:" + 
                        "\r\n matching.exe --input IMAGE1 IMAGE2 " + 
                        "--output RESULT " + 
                        "[--iterations NR_OF_IT]");
                }
                else if (param.ContainsKey("input") && 
                    param["input"].Count == 2 &&
                    param.ContainsKey("output") && 
                    param["output"].Count == 1)
                {
                    try
                    {
                        int it = 1000;
                        if (param.ContainsKey("iterations") &&
                            param["iterations"].Count == 1)
                            it = Convert.ToInt32(
                                param["iterations"].ElementAt(0));

                        Matching.match(param["input"].ElementAt(0),
                            param["input"].ElementAt(1),
                            param["output"].ElementAt(0),
                            it);
                    }
                    catch
                    {
                        System.Console.WriteLine("Please use a numeric value " +
                            "for the parameter \"iterations\"");
                    }
                }
                else
                {
                    runGUI();
                }          
            }
        }

        /// <summary>
        /// Startet die grafische Oberfläche (für den Fall, dass keine Argumente
        /// übergeben werden).
        /// </summary>
        private static void runGUI()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new gui());
        }
    }
}
